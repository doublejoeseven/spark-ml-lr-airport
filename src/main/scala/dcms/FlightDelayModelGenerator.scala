package dcms

import org.apache.spark._

import org.apache.spark.rdd.RDD
import org.apache.spark.sql.SQLContext
import org.apache.spark.sql.types.{ StructType, StructField, DoubleType, StringType }

import org.apache.spark.ml.evaluation.BinaryClassificationEvaluator
import org.apache.spark.ml.classification.{BinaryLogisticRegressionSummary, LogisticRegression, LogisticRegressionModel}
import org.apache.spark.ml.tuning.{CrossValidator, ParamGridBuilder}
import org.apache.spark.ml.{Pipeline, PipelineModel}
import org.apache.spark.sql.functions._
import org.apache.spark.sql.functions.udf
import org.apache.spark.sql.functions.col
import org.apache.spark.ml.feature.{Binarizer, StringIndexer, VectorAssembler}
import org.apache.spark.sql.Row
import org.apache.spark.sql.Dataset
import java.time.Instant
import java.text.SimpleDateFormat
import java.util.Date

/*
 *
  Year,Month,DayofMonth,DayOfWeek,DepTime,
  CRSDepTime,ArrTime, CRSArrTime,UniqueCarrier,FlightNum,
  TailNum,ActualElapsedTime, CRSElapsedTime,AirTime,ArrDelay,
  DepDelay,Origin,Dest,Distance, TaxiIn,
  TaxiOut,Cancelled,CancellationCode,Diverted,CarrierDelay,
  WeatherDelay,NASDelay,SecurityDelay,LateAircraftDelay
  airline data for 2007,2008
 * Array(2007,1,1,1,1232,1225,1341,1340,WN,2891,N351,69,75,54,1,7,SMF,ONT,389,4,11,0,,0,0,0,0,0,0 )

 */

object FlightDelayModelGenerator {

  def main(args: Array[String]) {
    
    val startTime = new Date()
    val inputFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
    
    println(s"Modeling start: ${inputFormat.format(startTime)}")

    
    var (filePath, ohareOnly, runMode) = getRunConfig()

    val conf = new SparkConf().setAppName("flight-delay")
    val sc = new SparkContext(conf)
    val sqlContext = new SQLContext(sc)
    val customSchema = StructType(Array(
      //Year,Month,DayofMonth,DayOfWeek,DepTime,
      StructField("Year", DoubleType, true),
      StructField("Month", DoubleType, true),
      StructField("DayofMonth", DoubleType, true),
      StructField("DayOfWeek", DoubleType, true),
      StructField("DepTime", DoubleType, true),
      //CRSDepTime,ArrTime, CRSArrTime,UniqueCarrier,FlightNum,
      StructField("CRSDepTime", DoubleType, true),
      StructField("ArrTime", DoubleType, true),
      StructField("CRSArrTime", DoubleType, true),
      StructField("UniqueCarrier", StringType, true),
      StructField("FlightNum", DoubleType, true),
      //TailNum,ActualElapsedTime, CRSElapsedTime,AirTime,ArrDelay,
      StructField("TailNum", StringType, true),
      StructField("ActualElapsedTime", DoubleType, true),
      StructField("CRSElapsedTime", DoubleType, true),
      StructField("AirTime", DoubleType, true),
      StructField("ArrDelay", DoubleType, true),
      //DepDelay,Origin,Dest,Distance, TaxiIn,
      StructField("DepDelay", DoubleType, true),
      StructField("Origin", StringType, true),
      StructField("Dest", StringType, true),
      StructField("Distance", DoubleType, true),
      StructField("TaxiIn", DoubleType, true),
      //TaxiOut,Cancelled,CancellationCode,Diverted,CarrierDelay,
      StructField("TaxiOut", DoubleType, true),
      StructField("Cancelled", DoubleType, true),
      StructField("CancellationCode", StringType, true),
      StructField("Diverted", DoubleType, true),
      StructField("CarrierDelay", DoubleType, true),
      //WeatherDelay,NASDelay,SecurityDelay,LateAircraftDelay
      StructField("WeatherDelay", DoubleType, true),
      StructField("NASDelay", DoubleType, true),
      StructField("SecurityDelay", DoubleType, true),
      StructField("LateAircraftDelay", DoubleType, true)))

    val df = sqlContext.read.format("csv").option("header", "true").schema(customSchema).load(filePath)

   //convert column to binary.  1  is delayed 0 is on-time
     val binarizer = new Binarizer().setInputCol("DepDelay").setOutputCol("DelayIndicator").setThreshold(15)
	  //val binarizedDf = binarizer.transform(df)
    
    //StringIndexer will make these text fields available for use in the model by creating an index for their string values
    val labelIndexer = new StringIndexer().setInputCol("DelayIndicator").setOutputCol("label").setHandleInvalid("skip")
    val carrierIndexer = new StringIndexer().setInputCol("UniqueCarrier").setOutputCol("AirlineId").setHandleInvalid("skip")
    val planeIndexer = new StringIndexer().setInputCol("TailNum").setOutputCol("PlaneId").setHandleInvalid("skip")
    val originIndexer = new StringIndexer().setInputCol("Origin").setOutputCol("OriginId").setHandleInvalid("skip")
    val destinationIndexer = new StringIndexer().setInputCol("Dest").setOutputCol("DestId").setHandleInvalid("skip")
    val featureCols = Array("Month", "DayofMonth", "DayOfWeek", "DepTime", "Distance", "FlightNum", "AirlineId", "PlaneId", "OriginId", "DestId")
    val assembler = new VectorAssembler().setInputCols(featureCols).setOutputCol("features").setHandleInvalid("skip")
    val lr = new LogisticRegression().setMaxIter(10).setRegParam(0.3).setLabelCol("label").setFeaturesCol("features").setFamily("binomial")
    val pipeline = new Pipeline().setStages(Array(binarizer, labelIndexer, carrierIndexer, planeIndexer, originIndexer, destinationIndexer, assembler, lr))
    val pipelineModel = pipeline.fit(df.filter("Year=2007"))
    val model = pipelineModel.stages.last.asInstanceOf[LogisticRegressionModel]
    
    println(s"Coefficients: ${model.coefficients} Intercept: ${model.intercept}")
    var testData: String = "Year=2008"
    if ("Y".equals(ohareOnly)) {
      testData = testData + " and Origin='ORD'"
    }
    println(s"Predictions start: ${inputFormat.format(new Date())}")
    val predictions = pipelineModel.transform(df.filter(testData))
    println(s"Predictions end: ${inputFormat.format(new Date())}")

    println(s"Total # of Predictions ${predictions.count}")
    predictions.select("DelayIndicator", "label", "prediction").show(20)

    val trainingSummary = model.summary
    val objectiveHistory = trainingSummary.objectiveHistory
    //objectiveHistory.foreach(loss => println(loss))

    val binarySummary = trainingSummary.asBinary

    // Obtain the receiver-operating characteristic as a dataframe and areaUnderROC.
    val roc = binarySummary.roc
    //roc.show()
    //println(binarySummary.areaUnderROC)

    // Set the model threshold to maximize F-Measure
    val fMeasure = binarySummary.fMeasureByThreshold
    val fm = fMeasure.col("F-Measure")
    val maxFMeasure = fMeasure.select(max("F-Measure")).head().getDouble(0)
    val bestThreshold = fMeasure.where(col("F-Measure") === maxFMeasure).select("threshold").head().getDouble(0)
    model.setThreshold(bestThreshold)

    val evaluator = new BinaryClassificationEvaluator().setLabelCol("label")
    printSummary(evaluator, predictions)

    var modelPath = "model" + java.io.File.separatorChar + "mymodel.bin-" + Instant.now.getEpochSecond
    if (!"LOCAL".equals(runMode)) {
      filePath = "hdfs://ip-172-31-85-149.ec2.internal:9000/model/flightDelay.bin-" + Instant.now.getEpochSecond
    }
    model.save(modelPath)
    val execTime = (new Date().getTime - startTime.getTime)/1000
    println("Modeling end: " + inputFormat.format(new Date()))
    println("Took [" + execTime + "] seconds")

  }

  /**
   * Converts DepDelay to binary 
   *  Creates StringIndex for DelayIndicator
   * Creates StringIndex for UniqueCarrier
   * Creates StringIndex for TailNum
   * Creates StringIndex for Origin
   * Creates StringIndex for Dest
   */
  def applyIndices(df: org.apache.spark.sql.DataFrame) = {//convert column to binary.  1  is delayed 0 is on-time
    val binarizer = new Binarizer().setInputCol("DepDelay").setOutputCol("DelayIndicator").setThreshold(15)
    val binarizedDf = binarizer.transform(df)

    //StringIndexer will make these text fields available for use in the model by creating an index for their string values
    val labelIndexer = new StringIndexer().setInputCol("DelayIndicator").setOutputCol("label").setHandleInvalid("skip")
    val carrierIndexer = new StringIndexer().setInputCol("UniqueCarrier").setOutputCol("AirlineId").setHandleInvalid("skip")
    val planeIndexer = new StringIndexer().setInputCol("TailNum").setOutputCol("PlaneId").setHandleInvalid("skip")
    val originIndexer = new StringIndexer().setInputCol("Origin").setOutputCol("OriginId").setHandleInvalid("skip")
    val destinationIndexer = new StringIndexer().setInputCol("Dest").setOutputCol("DestId").setHandleInvalid("skip")

    //Fit data with new columns
    val df2 = carrierIndexer.fit(binarizedDf).transform(binarizedDf)
    val df3 = planeIndexer.fit(df2).transform(df2)
    val df4 = originIndexer.fit(df3).transform(df3)
    val df5 = destinationIndexer.fit(df4).transform(df4)
    val df6 = labelIndexer.fit(df5).transform(df5)
    df6
  }

  def printSummary(evaluator: org.apache.spark.ml.evaluation.BinaryClassificationEvaluator, predictions: org.apache.spark.sql.DataFrame) = {
    val accuracy = evaluator.evaluate(predictions)
    println("accuracy/area under ROC: " + accuracy)

    val lp = predictions.select("label", "prediction")
    val counttotal = predictions.count()
    //correct predictions!
    val correct = lp.filter(col("label") === col("prediction")).count()
    //wrong predictions
    val wrong = lp.filter(not(col("label") === col("prediction"))).count()
    //number of times model correctly predicted a delay
    val truep = lp.filter(col("prediction") === 0.0).filter(col("label") === col("prediction")).count()
    //number of times model incorrectly predicted a flight would be on time
    val falseN = lp.filter(col("prediction") === 0.0).filter(not(col("label") === col("prediction"))).count()
    //number of times model incorrectly predicted a flight would be delayed but was on time
    val falseP = lp.filter(col("prediction") === 1.0).filter(not(col("label") === col("prediction"))).count()
    val ratioWrong = wrong.toDouble / counttotal.toDouble
    val ratioCorrect = correct.toDouble / counttotal.toDouble

    println("Total Number of Predictions: " + counttotal
      + "\nCorrect Predictions: " + correct
      + "\nWrong Predictions: " + wrong
      + "\nTrue+ Number of times model correctly predicted a delay: " + truep
      + "\nfalse- Number of times model incorrectly predicted a flight would be on time: " + falseN
      + "\nfalse+ Number of times model incorrectly predicted a flight would be delayed but was on time: " + falseP
      + "\nIncorrect Prediction Ratio: " + ratioWrong
      + "\nCorrect Prediction Ratio: " + ratioCorrect)
  }

  def getRunConfig() = {
    val runModeOpt = sys.env.get("RUN_MODE")
    val runMode = runModeOpt.getOrElse("LOCAL")
    val ohareOnlyModeOpt = sys.env.get("OHARE_ONLY")
    val ohareOnly = ohareOnlyModeOpt.getOrElse("Y")
    val filePathOpt = sys.env.get("FILE_PATH")
    var filePath = filePathOpt.getOrElse("data/*.csv")
    if (!"LOCAL".equals(runMode)) {
      filePath = "hdfs://ip-172-31-85-149.ec2.internal:9000/" + filePath
    }
    
    println("Run Mode: " + runMode)
    println("Ohare Only?: " + ohareOnly)
    println("data files: " + filePath)
    (filePath, ohareOnly, runMode)
  }
}

