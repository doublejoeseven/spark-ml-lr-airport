#!/bin/bash

export JAVA_HOME=/usr/lib/jvm/java-1.8.0-openjdk-1.8.0.201.b09-0.amzn2.x86_64
export SPARK_HOME=/opt/spark
export PATH=$JAVA_HOME/bin:$SPARK_HOME/bin:$PATH
export RUN_MODE=LOCAL
export RANDOM_SPLIT=N
mvn clean install

spark-submit --class dcms.FlightDelayModelGenerator \
./target/sparkairportlab-1.0.jar