import org.apache.spark._

import org.apache.spark._

import org.apache.spark.rdd.RDD
import org.apache.spark.sql.SQLContext
import org.apache.spark.sql.types.{StructType, StructField, DoubleType, StringType}


import org.apache.spark.ml.feature.StringIndexer
import org.apache.spark.ml.feature.VectorAssembler
import org.apache.spark.ml.evaluation.BinaryClassificationEvaluator
import org.apache.spark.ml.classification.{LogisticRegression, LogisticRegressionModel, BinaryLogisticRegressionSummary}
import org.apache.spark.ml.tuning.{CrossValidator, ParamGridBuilder}
import org.apache.spark.ml.{Pipeline, PipelineModel}
import org.apache.spark.ml.feature.VectorAssembler
import org.apache.spark.sql.functions._
import org.apache.spark.sql.functions.udf
import org.apache.spark.sql.functions.col
import org.apache.spark.ml.feature.{Binarizer, StringIndexer}
import org.apache.spark.sql.Row
import org.apache.spark.sql.Dataset
import java.time.Instant
import java.text.SimpleDateFormat
import java.util.Date

val sqlContext = new org.apache.spark.sql.SQLContext(sc)
val inputFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")

 val customSchema = StructType(Array(
            //Year,Month,DayofMonth,DayOfWeek,DepTime,
            StructField("Year", DoubleType, true), 
            StructField("Month", DoubleType, true),
            StructField("DayofMonth", DoubleType, true), 
            StructField("DayOfWeek", DoubleType, true),
            StructField("DepTime", DoubleType, true),
            //CRSDepTime,ArrTime, CRSArrTime,UniqueCarrier,FlightNum,
            StructField("CRSDepTime", DoubleType, true),
            StructField("ArrTime", DoubleType, true),
            StructField("CRSArrTime", DoubleType, true),
            StructField("UniqueCarrier", StringType, true),
            StructField("FlightNum", DoubleType, true),
            //TailNum,ActualElapsedTime, CRSElapsedTime,AirTime,ArrDelay,
            StructField("TailNum", StringType, true),
            StructField("ActualElapsedTime", DoubleType, true),
            StructField("CRSElapsedTime", DoubleType, true),
            StructField("AirTime", DoubleType, true),
            StructField("ArrDelay", DoubleType, true),
            //DepDelay,Origin,Dest,Distance, TaxiIn,
            StructField("DepDelay", DoubleType, true),
            StructField("Origin", StringType, true),
            StructField("Dest", StringType, true),
            StructField("Distance", DoubleType, true),
            StructField("TaxiIn", DoubleType, true),
            //TaxiOut,Cancelled,CancellationCode,Diverted,CarrierDelay,
            StructField("TaxiOut", DoubleType, true),
            StructField("Cancelled", DoubleType, true),
            StructField("CancellationCode", StringType, true),
            StructField("Diverted", DoubleType, true),
            StructField("CarrierDelay", DoubleType, true),
            //WeatherDelay,NASDelay,SecurityDelay,LateAircraftDelay
            StructField("WeatherDelay", DoubleType, true),
            StructField("NASDelay", DoubleType, true),
            StructField("SecurityDelay", DoubleType, true),
            StructField("LateAircraftDelay", DoubleType, true)
            )
        )

    val df = sqlContext.read.format("csv").option("header", "true").schema(customSchema).load("hdfs://ip-172-31-85-149.ec2.internal:9000/data/*.csv")
   
    
    //convert column to binary.  1  is delayed 0 is on-time
    val binarizer = new Binarizer().setInputCol("DepDelay").setOutputCol("DelayIndicator").setThreshold(15)
	  //val binarizedDf = binarizer.transform(df)
    
    //StringIndexer will make these text fields available for use in the model by creating an index for their string values
    val labelIndexer = new StringIndexer().setInputCol("DelayIndicator").setOutputCol("label").setHandleInvalid("skip")
    val carrierIndexer = new StringIndexer().setInputCol("UniqueCarrier").setOutputCol("AirlineId").setHandleInvalid("skip")
    val planeIndexer = new StringIndexer().setInputCol("TailNum").setOutputCol("PlaneId").setHandleInvalid("skip")
    val originIndexer = new StringIndexer().setInputCol("Origin").setOutputCol("OriginId").setHandleInvalid("skip")
    val destinationIndexer = new StringIndexer().setInputCol("Dest").setOutputCol("DestId").setHandleInvalid("skip")
    val featureCols = Array("Month", "DayofMonth", "DayOfWeek", "DepTime", "Distance", "FlightNum", "AirlineId", "PlaneId", "OriginId", "DestId")
    val assembler = new VectorAssembler().setInputCols(featureCols).setOutputCol("features").setHandleInvalid("skip")
    val lr = new LogisticRegression().setMaxIter(10).setRegParam(0.3).setLabelCol("label").setFeaturesCol("features").setFamily("binomial")
    val pipeline = new Pipeline().setStages(Array(binarizer, labelIndexer, carrierIndexer, planeIndexer, originIndexer, destinationIndexer, assembler, lr))
    val pipelineModel = pipeline.fit(df.filter("Year=2007"))
    //val model = cvModel.bestModel.asInstanceOf[PipelineModel].stages.last.asInstanceOf[LogisticRegressionModel]
    val model = pipelineModel.stages.last.asInstanceOf[LogisticRegressionModel]
    
    
    
    println("Training Model start: " + inputFormat.format(new Date()))
   
    println("Training Complete end: " + inputFormat.format(new Date()))
    println(s"Coefficients: ${model.coefficients} Intercept: ${model.intercept}")
    
    println("Predictions start: "+inputFormat.format(new Date()))
    val predictions = pipelineModel.transform(df.filter("Year=2008"))
    println("Predictions end: "+inputFormat.format(new Date()))

    println("Predictions ")
    predictions.select("DelayIndicator", "label", "prediction").show(20)

    val trainingSummary = model.summary
    val objectiveHistory = trainingSummary.objectiveHistory
    //objectiveHistory.foreach(loss => println(loss))

    val binarySummary = trainingSummary.asBinary

    // Obtain the receiver-operating characteristic as a dataframe and areaUnderROC.
    val roc = binarySummary.roc
    //roc.show()
    println(binarySummary.areaUnderROC)

    // Set the model threshold to maximize F-Measure
    val fMeasure = binarySummary.fMeasureByThreshold
    val fm = fMeasure.col("F-Measure")
    val maxFMeasure = fMeasure.select(max("F-Measure")).head().getDouble(0)
    val bestThreshold = fMeasure.where(col("F-Measure") === maxFMeasure).select("threshold").head().getDouble(0)
    model.setThreshold(bestThreshold)

    val evaluator = new BinaryClassificationEvaluator().setLabelCol("label")
   
    val accuracy = evaluator.evaluate(predictions)
    println("accuracy: "+ accuracy)

    val lp = predictions.select( "label", "prediction")
    val counttotal = predictions.count()
    //correct predictions!
    val correct = lp.filter(col("label") === col("prediction")).count()
    //wrong predictions
    val wrong = lp.filter(not(col("label") === col("prediction"))).count()
    //number of times model correctly predicted a delay 
    val truep = lp.filter(col("prediction") === 0.0).filter(col("label") === col("prediction")).count()
    //number of times model incorrectly predicted a flight would be on time
    val falseN = lp.filter(col("prediction") === 0.0).filter(not(col("label") === col("prediction"))).count()
    //number of times model incorrectly predicted a flight would be delayed but was on time
    val falseP = lp.filter(col("prediction") === 1.0).filter(not(col("label") === col("prediction"))).count()
    val ratioWrong=wrong.toDouble/counttotal.toDouble
    val ratioCorrect=correct.toDouble/counttotal.toDouble

    println("Total Number of Predictions: "+counttotal
    +"\nCorrect Predictions: "+correct
    +"\nWrong Predictions: "+wrong
    +"\nTrue+ Number of times model correctly predicted a delay: "+truep
    +"\nfalse- Number of times model incorrectly predicted a flight would be on time: "+falseN
    +"\nfalse+ Number of times model incorrectly predicted a flight would be delayed but was on time: "+falseP
    +"\nIncorrect Prediction Ratio: "+ratioWrong
    +"\nCorrect Prediction Ratio: "+ratioCorrect)
   
