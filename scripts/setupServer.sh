#!/bin/bash
sudo yum install -y wget git java-1.8.0-openjdk-devel maven
wget https://archive.apache.org/dist/spark/spark-2.4.1/spark-2.4.1-bin-hadoop2.7.tgz  
wget https://archive.apache.org/dist/hadoop/common/hadoop-2.7.7/hadoop-2.7.7.tar.gz
# Unpack Spark in the /opt directory
sudo tar zxvf spark-2.4.1-bin-hadoop2.7.tgz -C /opt
# Unpack hadoop
tar -zxvf hadoop-2.7.7.tar.gz -C /opt
# Update permissions on installation
sudo chown -R ec2-user:ec2-user /opt/spark-2.4.1-bin-hadoop2.7
# Create a symbolic link to make it easier to access
sudo ln -s /opt/spark-2.4.1-bin-hadoop2.7 /opt/spark 