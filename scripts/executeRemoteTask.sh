#!/bin/bash

export JAVA_HOME=/usr/lib/jvm/java-1.8.0-openjdk-1.8.0.201.b09-0.amzn2.x86_64
export SPARK_HOME=/opt/spark
export PATH=$JAVA_HOME/bin:$SPARK_HOME/bin:$PATH
export RUN_MODE=REMOTE
export RANDOM_SPLIT=Y
mvn clean install

spark-submit --class dcms.FlightDelayModelGenerator \
--master spark://ec2-3-212-107-100.compute-1.amazonaws.com:7077 ./target/sparkairportlab-1.0.jar